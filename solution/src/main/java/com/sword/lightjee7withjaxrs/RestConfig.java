/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sword.lightjee7withjaxrs;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author Alexis-Dionisis
 */
@ApplicationPath("/api")
public class RestConfig extends Application {
    
}
