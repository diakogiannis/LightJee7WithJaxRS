package com.sword.lightjee7withjaxrs.domain;

import com.sword.lightjee7withjaxrs.model.entity.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;

import static javax.ejb.TransactionAttributeType.REQUIRED;

@Singleton
public class PersonDao {

    @PersistenceContext
    EntityManager em;

    Logger log = LoggerFactory.getLogger(PersonDao.class);

   /* public List<Person> findAll(){
        return em.createQuery("select p from Person p").getResultList();
    }*/

    public List<Person> findAll(){
        return em.createQuery("from Person p left join fetch p.cat").getResultList();
    }

    @TransactionAttribute(REQUIRED)
    public Person save(final Person person) {
        log.info("saving user {}", person);
        em.merge(person);
        return person;
    }


}
