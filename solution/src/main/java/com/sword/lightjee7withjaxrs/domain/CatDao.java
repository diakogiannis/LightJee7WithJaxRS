package com.sword.lightjee7withjaxrs.domain;

import com.sword.lightjee7withjaxrs.model.entity.Cat;
import com.sword.lightjee7withjaxrs.model.entity.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static javax.ejb.TransactionAttributeType.REQUIRED;

@Singleton
public class CatDao {

    @PersistenceContext
    EntityManager em;

    Logger log = LoggerFactory.getLogger(CatDao.class);

    public List<Cat> findAll(){
        return em.createQuery("from Cat c").getResultList();
    }

    @TransactionAttribute(REQUIRED)
    public Cat save(final Cat cat) {
        log.info("saving cat {}", cat);
        em.merge(cat);
        return cat;
    }


}
