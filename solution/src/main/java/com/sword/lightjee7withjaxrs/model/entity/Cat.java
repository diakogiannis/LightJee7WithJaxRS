package com.sword.lightjee7withjaxrs.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class Cat implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String catName;

/*    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id")*/
    private Person person;

    public Cat(){
        //default constructor
    }

    public Cat(Long id, String catName) {
        this.id = id;
        this.catName = catName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "id=" + id +
                ", catName='" + catName + '\'' +
                ", person=" + person +
                '}';
    }
}
