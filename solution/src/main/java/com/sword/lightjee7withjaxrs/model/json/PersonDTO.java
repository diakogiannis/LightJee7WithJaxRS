package com.sword.lightjee7withjaxrs.model.json;

import com.sword.lightjee7withjaxrs.model.entity.Cat;

import java.io.Serializable;
import java.util.List;

public class PersonDTO implements Serializable {


    private String firstname;

    private String lastname;

    private List<Cat> cat;

    public PersonDTO() {
        //default constructor
    }


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public List<Cat> getCat() {
        return cat;
    }

    public void setCat(List<Cat> cat) {
        this.cat = cat;
    }

    @Override
    public String toString() {
        return "PersonDTO{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", cat=" + cat +
                '}';
    }
}
