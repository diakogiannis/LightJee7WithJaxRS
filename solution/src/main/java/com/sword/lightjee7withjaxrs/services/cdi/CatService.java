package com.sword.lightjee7withjaxrs.services.cdi;

import com.sword.lightjee7withjaxrs.domain.CatDao;
import com.sword.lightjee7withjaxrs.domain.PersonDao;
import com.sword.lightjee7withjaxrs.model.entity.Cat;
import com.sword.lightjee7withjaxrs.model.entity.Person;
import com.sword.lightjee7withjaxrs.model.json.PersonDTO;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class CatService {

    Logger log = LoggerFactory.getLogger(CatService.class);

    @EJB
    CatDao catRepository;

    public List<Cat> findAll(){
        return catRepository.findAll();
    }

    public Cat save(Cat c){
        return catRepository.save(c);
    }




}
