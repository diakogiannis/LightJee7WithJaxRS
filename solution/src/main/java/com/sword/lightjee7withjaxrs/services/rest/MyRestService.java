/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sword.lightjee7withjaxrs.services.rest;

import com.sword.lightjee7withjaxrs.model.entity.Cat;
import com.sword.lightjee7withjaxrs.model.entity.Person;
import com.sword.lightjee7withjaxrs.model.json.PersonDTO;
import com.sword.lightjee7withjaxrs.services.cdi.CatService;
import com.sword.lightjee7withjaxrs.services.cdi.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Alexis-Dionisis
 */
@Stateful
@Path("/myservice")
public class MyRestService {

    Logger log = LoggerFactory.getLogger(MyRestService.class);

    @EJB
    PersonService personService;

    @EJB
    CatService catService;

    @GET
    @Path("/demo")
    // /api/myservice/simple
    @Produces(MediaType.APPLICATION_JSON)
    public Response mySimpleRest(){
        Person p = new Person(1l,"Alexius","Diakogiannis");


        List cats = new ArrayList();

        Cat c = new Cat(1l,"Rosy");
        cats.add(c);


        c = new Cat(1l,"Boubou");
        cats.add(c);

        p.setCat(cats);


        return Response.ok().entity(p).build();
    }
    



    @GET
    @Path("/cat")
    @Produces(MediaType.APPLICATION_JSON)
    public Response showCats(){
       return Response.ok().entity(catService.findAll()).build();
    }

    @POST
    @Path("/cat")
    public Response saveCats(Cat c){
        try{
            catService.save(c);
            return Response.accepted().build();
        }catch (Exception e){
            log.error(e.getMessage(),e);
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/person")
    @Produces(MediaType.APPLICATION_JSON)
    public Response showPersons(){
        return Response.ok().entity(personService.findAllPersons()).build();
}

    @POST
    @Path("/person")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPerson(PersonDTO p){
        try{
            personService.save(p);
            return Response.accepted().build();
        }catch (Exception e){
            log.error(e.getMessage(),e);
            return Response.serverError().build();
        }
    }


    
}
