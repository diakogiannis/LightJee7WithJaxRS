package com.sword.lightjee7withjaxrs.services.cdi;

import com.sword.lightjee7withjaxrs.domain.PersonDao;
import com.sword.lightjee7withjaxrs.model.entity.Person;
import com.sword.lightjee7withjaxrs.model.json.PersonDTO;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class PersonService {

    Logger log = LoggerFactory.getLogger(PersonService.class);

    @EJB
    PersonDao userRepository;

    public List<Person> findAllPersons(){
        return userRepository.findAll();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void save(final PersonDTO personDTO){
        List myMappingFiles = new ArrayList();
        myMappingFiles.add("dozerMapping.xml");
        DozerBeanMapper mapper = new DozerBeanMapper();
        mapper.setMappingFiles(myMappingFiles);
        log.info("received "+personDTO.toString());
        Person person = mapper.map(personDTO, Person.class);
        userRepository.save(person);
    }

}
