/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sword.lightjee7withjaxrs.services.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateful;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Alexis-Dionisis
 */
@Stateful
@Path("/myservice")
public class MyRestService {

    Logger log = LoggerFactory.getLogger(MyRestService.class);


    @GET
    @Path("/demo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response mySimpleRest(){
                return Response.ok().entity("OK").build();
    }
    





    
}
